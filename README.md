Implementation of cleanup inactive users on site.

Requirements
All users that not logged in some period should recive several notifications and after last one it will be blocked on site.
System should have configuration form for periods and notification text.
Works should be done by queue manager and triggered by cron.

Implementation
On each cron run we select users filterd by last login period based on stages configuration. After some checks we added them to the special queue. This queue processed in separate handler and sent emails to users with configured text. Also we block users on cron event who have last login date more then last stage period and directly blocked them.

Instruction
We have admin page at /admin/ssp-config/inactive-users with all configs. You can setup stage parameters like start and end period and text (see textarea description). 
You can change the text of provided email for this event and use some tokens in it (check textarea description).
You can select user role to exclude from these checks
